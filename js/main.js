let listTodo = [];
let listCompleted = [];
const TODO_DATA = "TODO";
const TODO_COMPLETED = "COMPLETED";

// Lưu local Store
function saveLocalStorage() {
  let todo = JSON.stringify(listTodo);
  localStorage.setItem(TODO_DATA, todo);

  let complete = JSON.stringify(listCompleted);
  localStorage.setItem(TODO_COMPLETED, complete);
}
function loadLocalStorage() {
  let rawTodo = localStorage.getItem(TODO_DATA);
  if (rawTodo !== null) {
    listTodo = JSON.parse(rawTodo);
  }

  let rawCompleted = localStorage.getItem(TODO_COMPLETED);
  if (rawCompleted !== null) {
    listCompleted = JSON.parse(rawCompleted);
  }

  renderList();
}

// Render ra HTML
const renderList = () => {
  let contentTodo = "";
  let contentComplete = "";

  listTodo.forEach((item, index) => {
    contentTodo += `
      <li>
      <span>${index + 1}. ${item}</span>
      <span class="buttons">
      <button class="remove" onclick="removeItemTodo('${item}')">
      <i class="fa fa-times"></i>

      </button>
      <button class="complete" onclick="completeItem('${item}')">
        <i class="fa fa-check-circle"></i>
      </button>
      </span>
      </li>`;
  });

  listCompleted.forEach((item, index) => {
    contentComplete += `
      <li>
          <span><i class="fa fa-check"></i> ${item}</span>
          <span class="buttons">
            <button class="remove" onclick="removeItemComplete('${item}')">
              <i class="fa fa-eraser"></i>
            </button>
          </span>
      </li>
      `;
  });
  document.getElementById("todo").innerHTML = contentTodo;
  document.getElementById("completed").innerHTML = contentComplete;
};

loadLocalStorage();

// Add Activity Button
const addItem = () => {
  let text = document.getElementById("newTask").value;
  // Nếu input không bị trống
  if (text !== null) {
    listTodo.push(text);
    console.log("listTodo: ", listTodo);
    saveLocalStorage();
    renderList();
  }
};

const findIndex = (text, arr) => {
  return arr.indexOf(text);
};
const removeItemTodo = (text) => {
  let index = findIndex(text, listTodo);
  listTodo.splice(index, 1);
  saveLocalStorage();
  renderList();
};

const removeItemComplete = (text) => {
  let index = findIndex(text, listCompleted);
  listCompleted.splice(index, 1);
  saveLocalStorage();
  renderList();
};

const completeItem = (text) => {
  listCompleted.push(text);
  let index = findIndex(text, listTodo);
  listTodo.splice(index, 1);
  saveLocalStorage();
  renderList();
};

// Sort function

const sortAZ = () => {
  listTodo = listTodo.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
  });
  listCompleted = listCompleted.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
  });
  saveLocalStorage();
  renderList();
};

const sortZA = () => {
  listTodo = listTodo.reverse();
  listCompleted = listCompleted.reverse();
  saveLocalStorage();
  renderList();
};

// Functions
document.getElementById("addItem").addEventListener("click", addItem);
document.getElementById("two").addEventListener("click", sortAZ);
document.getElementById("three").addEventListener("click", sortZA);
window.removeItemTodo = removeItemTodo;
window.removeItemComplete = removeItemComplete;
window.completeItem = completeItem;
